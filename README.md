# Introduction

The company site is build using [Jekyll](https://jekyllrb.com/). Check out
their site for more information, configuration options and plugins.

## Run it locally

After you cloned this repository, you can follow the next two approaches
to run the comany site locally.

### Use Docker

If you have installed Docker locally, then this is the easiest way to run
it. Simply go to the root directory of the cloned repository and you
only have to run:

```bash
docker-compose up
```

You can access the site on: `http://localhost:4000`

### Use Ruby

You can also install Ruby (and RubyGems) yourself.
[Check the installation instructions on Jekyll.](https://jekyllrb.com/docs/installation/)

Once you have installed everything, you can run the following command on the
root directory of the repository:

```command
bundle exec jekyll serve --livereload
```

## Deployment

To be able to deploy the company site, we use the power of GitLab CI/CD,
Docker and Kubernetes. Using the Jekyll images we use Docker to build the
site. This build is copied to a Docker image which we push to our registry.
When this is done, we deploy this Docker container to Kubernetes. Also you
can pull this image to run the company site locally.
