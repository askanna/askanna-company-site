#!/usr/bin/env bash
#
# Usage:
#   bash script/htmlproofer.sh
#
# Inspired by Jekyll: https://github.com/jekyll/jekyll/blob/master/script/proof

set -e

function msg {
  printf "\e[0;37m==> $1\e[0m\n"
}

IGNORE_HREFS=$(ruby -e 'puts %w{
  localhost
  www.linkedin.com
  www.twitter.com
  twitter.com
}.map{|h| "/#{h}/"}.join(",")')

export PROOF=true
export NOKOGIRI_USE_SYSTEM_LIBRARIES=true

# 1.
msg "Building..."
bundle exec jekyll build --config _config.yml,_config_development.yml
printf "\n"

# 2.
msg "Proofing..."
bundle exec htmlproofer ./_site --check-favicon --check-html --check-opengraph --url-ignore $IGNORE_HREFS $@
