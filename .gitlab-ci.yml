variables:
  GCP_PROJECT: askanna-services
  GKE_CLUSTER_NAME: review-and-runners
  GKE_CLUSTER_ZONE: europe-west1-d
  URL_PRODUCTION: askanna.io
  DEVOPS_DOMAIN: askanna.dev
  DOCKER_BASE_IMAGE: $CI_REGISTRY_IMAGE/base:$CI_COMMIT_REF_SLUG
  DOCKER_REVIEW: $CI_REGISTRY_IMAGE/review:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA

stages:
  - build-base
  - test
  - build-deploy
  - deploy
  - publish
  - cleanup

# To use Kaniko extend the job and set variable for DOCKER_DESTINATION and DOCKER_FILE
.use-kaniko:
  tags:
    - kubernetes
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  before_script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $DOCKER_FILE --destination $DOCKER_DESTINATION

build base:
  stage: build-base
  extends:
    - .use-kaniko
  variables:
    DOCKER_FILE: $CI_PROJECT_DIR/docker/Dockerfile.base
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $DOCKER_FILE --destination $DOCKER_BASE_IMAGE
  rules:
    - if: $CI_COMMIT_BRANCH
      changes:
        - Gemfile
        - Gemfile.lock
        - docker/Dockerfile.base

test site:
  stage: test
  image: $DOCKER_BASE_IMAGE
  tags:
    - kubernetes
  script:
    - bash script/htmlproofer.sh
  allow_failure: true

build review:
  stage: build-deploy
  extends:
    - .use-kaniko
  variables:
    DOCKER_FILE: $CI_PROJECT_DIR/docker/Dockerfile
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $DOCKER_FILE --destination $DOCKER_REVIEW --build-arg BASE_IMAGE=$DOCKER_BASE_IMAGE
  only:
    - branches
  except:
    - main

build production:
  stage: build-deploy
  extends:
    - .use-kaniko
  variables:
    DOCKER_FILE: $CI_PROJECT_DIR/docker/Dockerfile
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $DOCKER_FILE --destination $CI_REGISTRY_IMAGE:latest --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA --build-arg BASE_IMAGE=$DOCKER_BASE_IMAGE
  only:
    - main

deploy review:
  stage: deploy
  image: gitlab.askanna.io:4567/askanna/services/helm
  tags:
    - kubernetes
  script:
    - init_k8s
    - helm upgrade
      --install
      --set app.image="$DOCKER_REVIEW"
      --set app.host="$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG.$DEVOPS_DOMAIN"
      --set deployment.minReplicas=1
      --wait
      $CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG
      ./k8s-chart
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG.$DEVOPS_DOMAIN
    on_stop: stop and delete review
    auto_stop_in: 1 week
  only:
    - branches
  except:
    - main

deploy production:
  stage: deploy
  image: gitlab.askanna.io:4567/askanna/services/helm
  tags:
    - kubernetes
  script:
    - init_k8s
    - helm upgrade
      --install
      --set app.image="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
      --set app.host="$URL_PRODUCTION"
      --wait
      $CI_PROJECT_NAME-production
      ./k8s-chart
  environment:
    name: production
    url: https://$URL_PRODUCTION
  only:
    - main

publish on GitLab.com:
  stage: publish
  tags:
    - kubernetes
  before_script:
    - apt-get update -y && apt-get install openssh-client git curl -y
    - curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
    - apt-get install git-lfs -y
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY_GITLAB_COM}" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - git config --global user.email "hello@askanna.io"
    - git config --global user.name "AskAnna Robot"
  script:
    - git remote add gitlab git@gitlab.com:askanna/askanna-company-site.git
    - git push -u gitlab HEAD:main
  allow_failure: true
  only:
    - main


stop and delete review:
  stage: cleanup
  image: gitlab.askanna.io:4567/askanna/services/helm
  tags:
    - kubernetes
  variables:
    GIT_STRATEGY: none
  script:
    - init_k8s
    - helm uninstall $CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  when: manual
  allow_failure: true
  only:
    - branches
  except:
    - main

.functions: &functions |
    # Functions
    function init_k8s() {
        gcloud auth activate-service-account --project $GCP_PROJECT --key-file $GCP_SERVICE_ACCOUNT_FILE_AA_SERVICES
        gcloud container clusters get-credentials $GKE_CLUSTER_NAME --zone $GKE_CLUSTER_ZONE --project $GCP_PROJECT
    }

before_script:
  - *functions
