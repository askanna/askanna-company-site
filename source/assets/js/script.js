const menuToggle = document.getElementById('burger');
if (menuToggle) {
  menuToggle.addEventListener('change', function () {
    const el = document.querySelector('.menu');
    if (this.checked) {
      el.classList.remove('mb--hidden');
    } else {
      el.classList.add('mb--hidden');
    }
  });
}

const moreLink = document.querySelector('.js-more');
if (moreLink) {
  moreLink.addEventListener('click', function (e) {
    e.preventDefault();
    const moreAnchor = document.querySelector('#demo');
    moreAnchor.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
      inline: 'nearest'
    });
  });
}

const joinLink = document.querySelector('.js-join');
if (joinLink) {
  joinLink.addEventListener('click', function (e) {

    e.preventDefault();
    console.log("test");
    const joinAnchor = document.querySelector('#join');
    joinAnchor.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
      inline: 'nearest'
    });
  });
}

const manifestoLink = document.querySelector('.js-manifesto');
if(manifestoLink){
  manifestoLink.addEventListener('click', function (e) {
    e.preventDefault();
    const manifestoAnchor = document.querySelector('#manifesto');
    manifestoAnchor.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest'
    });
  });
}
