let copy_button_text ='<svg style="width:20px;height:20px" viewBox="0 0 24 24"><path fill="currentColor" d="M19,21H8V7H19M19,5H8A2,2 0 0,0 6,7V21A2,2 0 0,0 8,23H19A2,2 0 0,0 21,21V7A2,2 0 0,0 19,5M16,1H4A2,2 0 0,0 2,3V17H4V3H16V1Z" /></svg>'
let copy_button_confirm_text = '<svg style="width:20px;height:20px" viewBox="0 0 24 24"><path fill="currentColor" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z" /></svg>'

let codes = document.querySelectorAll('.highlight > pre > code');
let countID = 0;
codes.forEach((code) => {

  code.setAttribute("id", "code" + countID);

  let btn = document.createElement('button');
  btn.innerHTML = copy_button_text;
  btn.className = "btn-copy";
  // btn.setAttribute("data-clipboard-action", "copy");
  btn.setAttribute("data-clipboard-target", "#code" + countID);

  let span = document.createElement('span');
  span.appendChild(btn);
  span.className = "btn-copy";

  code.before(span);

  countID++;
});

let clipboard = new ClipboardJS('.btn-copy');

clipboard.on('success', function(e) {
  e.clearSelection();

  e.trigger.innerHTML = copy_button_confirm_text;

  setTimeout(function(){
    e.trigger.innerHTML = copy_button_text;
  }, 2000);
});
