---
title: First Open Source release of the AskAnna Backend
author: robbert
excerpt:
  We are excited to share that all the tools needed to run AskAnna are now open source available. A major milestone for our team and we are proud to share this news.
image: /assets/img/2023/github-askanna-backend-v0-27-0.png
image_header: True
image_tag: First Open Source release of the AskAnna Backend
image_url: https://github.com/askanna-io/askanna-backend/releases/tag/v0.27.0
---

We are excited to share that all the tools needed to run AskAnna are now open source available. A major milestone for
our team and we are proud to share this news.

At AskAnna, one of [our promises](/promise/#keep-your-freedom) is that you can keep your freedom. With the ability to
self-host AskAnna without paying any license fee, we provide a solution that eliminates vendor lock-in.

We also push for more transparency. In the field of AI, where companies like OpenAI demonstrate that they are not
transparent, we believe it's crucial to show that it can be done differently. By sharing our source code, we
demonstrate our commitment to transparency.

We could not build AskAnna without the thriving ecosystem of open source software. Our platform is built upon the
[Python](https://www.python.org/) [Django REST Framework](https://www.django-rest-framework.org/),
[Vue(tify)](https://vuetifyjs.com/en/), [Docker](https://www.docker.com/) and other open source tools. By embracing
open source, we enable you to use AskAnna without vendor lock-in.

The open source ecosystem not only makes development more accessible and cost-effective, but it also allows us to
learn, gain insights and share knowledge. We invite you to explore our source code and maybe learn something. Also, we
encourage and welcome your contributions to improve the capabilities of the AskAnna platform.

We take immense pride in our achievements and are already working hard on the next release. In the upcoming version,
we are excited to introduce our very own file storage app.

Thank you for your support, and we look forward to embarking on this open source journey with you.
