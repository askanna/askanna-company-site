---
title: The AskAnna Frontend is open
author: robbert
excerpt:
  We are proud to share that the AskAnna Frontend is published as an open source project on GitLab and GitHub. It’s
  our follow-up on the promise that you can keep your freedom with AskAnna. It's also our way of being transparent.
image: /assets/img/2023/github-askanna-frontend.png
image_header: True
image_tag: AskAnna Frontend project on GitHub
image_url: https://github.com/askanna-io/askanna-frontend
---

We are proud to share that the AskAnna Frontend is published as an open source project on
[GitLab](https://gitlab.com/askanna/askanna-frontend) and [GitHub](https://github.com/askanna-io/askanna-frontend).
It’s our follow-up on the [promise](/promise/#keep-your-freedom) that you can keep your freedom with AskAnna. It's
also our way of being transparent.

The AskAnna Frontend project is published under [the BSD-3-Claus](https://opensource.org/license/bsd-3-clause/), which
is one of the approved licenses by the [Open Source Initiative](https://opensource.org/). This means that we are
transparent about our code and will also give you much freedom to use it without a requirement to pay a license fee to
AskAnna.

By making this part of our project open source, it's also possible that everyone can contribute to the project or find inspiration in how we did it.

Our frontend stack primarily uses [Vue.js](https://vuejs.org/) and leverages the Composition API for over 90% of the
components. We use [Pinia](https://pinia.vuejs.org/) for store management. Pinia is a modern and intuitive state
management system for Vue.

We utilize [Vuetify](https://vuetifyjs.com/), a popular UI component library for Vue, to provide a polished and
streamlined user interface. This library offers various customizable and reusable components, allowing us to rapidly
build and maintain a consistent design system.

To streamline development and building processes, we use [Vite](https://vitejs.dev/). Vite is a fast, modern,
lightweight build tool that provides instant hot module replacement, lightning-fast bundling and a seamless
development experience.

By utilizing this frontend stack, we have aimed to deliver a performant and reliable user experience that is both
developer-friendly and scalable.

Feel free to check out the repo to see how we set up the AskAnna Frontend project. And if you want to contribute, let
us know!
