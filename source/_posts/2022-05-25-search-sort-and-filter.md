---
title: Search, sort and filter projects - AskAnna
page_title: Search, sort and filter projects in AskAnna
author: robbert
excerpt:
  Embracing responsible AI, that's how the day started. Intuitively you would say that everyone wants AI to be applied
  responsibly. But what is responsible AI?
image: /assets/img/2022/askanna-project-filters.png
image_header: False
image_tag: Bias in = Bias out - A day at Women in Data Science
---

Sometimes you want to use features like filtering or sorting a list of projects. But most of the time you don't need it. At AskAnna we love clean interfaces and we don't like distractions. That's why we build search, sort and filter in such a way that we don't show them by default. And when you need them, you can unhide them with a single click.

{% image 2022/askanna-project-filters.gif alt='Project filters in AskAnna' %}

In the project and workspace lists, you'll find the filter icon on the right<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="display: inline-flex;height:1.125em;vertical-align: text-top;"><path d="M15 19.88c.04.3-.06.62-.29.83a.996.996 0 0 1-1.41 0L9.29 16.7a.989.989 0 0 1-.29-.83v-5.12L4.21 4.62a1 1 0 0 1 .17-1.4c.19-.14.4-.22.62-.22h14c.22 0 .43.08.62.22a1 1 0 0 1 .17 1.4L15 10.75v9.13M7.04 5 11 10.06v5.52l2 2v-7.53L16.96 5H7.04Z"></path></svg>.
If the icon is empty, no search or filter is active. When a search or filter is applied, the filter icon is
filled<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="display: inline-flex;height:1.125em;vertical-align: text-top;"><path d="M14 12v7.88c.04.3-.06.62-.29.83a.996.996 0 0 1-1.41 0l-2.01-2.01a.989.989 0 0 1-.29-.83V12h-.03L4.21 4.62a1 1 0 0 1 .17-1.4c.19-.14.4-.22.62-.22h14c.22 0 .43.08.62.22a1 1 0 0 1 .17 1.4L14.03 12H14Z"></path></svg>.
You can now easily see if a filter is active. Even if the search, sort and filter options are not visible.

Another update we've done is to make it easy to see if sorting or filtering has been applied. For example, when you
sort the list you can directly see which sorting is applied by looking at the button. In addition, when a filter is
active, we change the icon's color <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="display: inline-flex;height:1.125em;vertical-align: text-top;"><path d="M6,13H18V11H6M3,6V8H21V6M10,18H14V16H10V18Z"></path></svg>
to AskAnna purple.

{% image 2022/askanna-project-sort-focus.png alt='Project sort label in AskAnna' %}

The search, sort and filter updates are now available on explore workspaces, explore projects and the workspace
project list. With this update, we want to make some features available that you may need if your workspace has many
projects.

We tried to make these new features so that you don't get overwhelmed when working with AskAnna. If you open AskAnna for the first time, or if your workspace doesn’t contain many projects yet, you only see the new filter icon. We tried to keep it simple. Please, [let us know](mailto:hello@askanna.io) if you have any feedback or ideas.
