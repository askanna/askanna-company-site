---
title: How does Git work? - AskAnna
page_title: How does Git work?
excerpt:
  Time to learn how Git works. What is a commit? Is there a difference between Branches and Tags? And how does a merge
  work?
image: /assets/img/2021/git-object-database.jpeg
image_header: True
image_tag: Git - The Object Database
author: robbert
---

I have been using Git for years. For code versioning, it's fantastic. But for data science, it can be tricky if you accidentally add your "big" dataset. I know that Git is not built for data versioning, but why?

Time to learn how Git works. [Kabisa](https://www.kabisa.nl/) has written an excellent introduction for starters. If
you're also curious about how Git works, this is a great place to start:

- [Git Object Database](https://www.kabisa.nl/)
- [Git Branches & Tags](https://www.kabisa.nl/tech/git-content-based-addressing-branches-and-tags/)
- [Cherry-picking](https://www.kabisa.nl/tech/git-putting-the-knowledge-into-practice/)

I even learned a new cool Python trick. Octopus merges where you merge multiple branches into a single commit. Even
monster octopus merges occur:
[https://github.com/torvalds/linux/commit/2cde51fbd0f310c8a2c5f977e665c0ac3945b46d](https://github.com/torvalds/linux/commit/2cde51fbd0f310c8a2c5f977e665c0ac3945b46d) (merging 66
branches)

Now that I know more about Git and that we really can't solve the Git & data version control problem, it's time to
move on to Data Science Version Control. Let's see what AskAnna can do about this :)
