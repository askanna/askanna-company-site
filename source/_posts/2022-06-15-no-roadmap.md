---
title: No Roadmap - AskAnna
page_title: No Roadmap
author: robbert
excerpt:
  Why don’t you have a roadmap for AskAnna? What is your release cycle?
---

Why don’t you have a roadmap for AskAnna? What is your release cycle?

At AskAnna, we work on awesome features and make them available when they are good enough to ship. We continuously research and learn.
This gives us insight into what we think we should work on.
[This blog](https://world.hey.com/jason/don-t-defer-quality-aaa105e4) from
[Jason Fried](https://www.linkedin.com/in/jason-fried/) reminded me that we only ship a feature when we are happy
with it.

Finding the right balance between going for high quality and at the same time keeping focus, don’t make features too
big and shipping them within a reasonable time is something I learn to do better every day. I love this journey of
improving myself.

For how we work at AskAnna, I found inspiration in [the book Shape Up](https://basecamp.com/shapeup). If you want to
read more about this topic, I recommend reading this (free) online book.
