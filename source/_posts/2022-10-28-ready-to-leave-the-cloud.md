---
title: Are you ready to leave the cloud? - AskAnna
page_title: Are you ready to leave the cloud?
author: robbert
excerpt:
  Why? Because using the cloud comes at a price. For businesses, it's not about paying by sharing your sensitive data
  for the big tech companies to use. It's the amount you pay for using the services.
---

Why? Because using the cloud comes at a price. For businesses, it's not about paying by sharing your sensitive data
for the big tech companies to use. It's the amount you pay for using the services. Instead of running it yourself, you
pay a massive premium so you think you don't have to worry about managing software and servers.

Recently Basecamp [announced](https://world.hey.com/dhh/why-we-re-leaving-the-cloud-654b47e0) that they are leaving
the cloud:

> <i>Renting computers is (mostly) a bad deal for medium-sized companies like ours with stable growth. The savings
> promised in reduced complexity never materialized. So we're making our plans to leave.</i>

Leaving the cloud, will that be the new trend? It could be, and at least it's interesting to keep the option in mind.
Or maybe you should take it one step further and make it business-critical that whatever you do, you should always
have the freedom to leave your current supplier.

Making this business-critical might be more important than you think. I have heard stories from various teams
operating in multiple clouds. Not because they wanted to but because it was too hard to migrate all services to a new
provider. This comes at a huge cost, as these companies have to pay for and maintain multiple cloud stacks...and
probably some local servers as well.

With this in mind, I'm concerned about what's happening today with all the intelligent and practical services cloud
providers offer for machine learning. Indeed, it can help to get your solution into production faster. But at which
price? How easy is it to leave your current supplier if you use their tools to integrate your solution into other
platforms?

You have to take flexibility into account. If you're not flexible enough to switch suppliers, you accept the risk you
have to pay. Maybe even literally. What if your cloud provider decides to double the price? Can you move, or are you
the sitting duck?

With [AskAnna](https://askanna.io/), we challenge ourselves to build upon open source software. We are a small company
and use hosting from cloud providers. At the same time, we don't use cloud-specific services from big tech companies.
If we did this, it would make it hard to move our stack. We recently tested this by spinning up a full copy of AskAnna
in [Leaf Cloud](https://www.leaf.cloud/). Proof to us that we do indeed have the freedom to run AskAnna wherever we
want.

The next time you hear how practical Sagemaker, Azure ML or Vertex AI are, think about the risk of losing your
freedom. Make sure the solution you build will not lock you in. Don't become a sitting duck.

Keep it simple, but also keep your freedom!
