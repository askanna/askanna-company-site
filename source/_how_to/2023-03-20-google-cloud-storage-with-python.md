---
title: "Google Cloud Storage with Python"
external: https://docs.askanna.io/integrations/data-sources/google-cloud-storage/
excerpt:
  This example shows how to set up a connection to Google Cloud Storage using Python. We show how you can read files
  from and write files to this object storage.
---
