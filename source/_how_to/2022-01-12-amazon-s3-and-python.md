---
title: Read from and write to Amazon S3 with Python
external: https://docs.askanna.io/integrations/data-sources/amazon-s3/
excerpt:
  This example shows how to set up a connection to the Amazon S3 object storage using Python. 
---
