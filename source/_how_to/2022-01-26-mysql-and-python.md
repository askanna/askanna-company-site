---
title: Use MySQL with Python
external: https://docs.askanna.io/integrations/data-sources/mysql/
excerpt:
  In this example we show how to read from and write to MySQL with Python Pandas.
---
