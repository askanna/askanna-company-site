---
title: Use variables to configure notifications | AskAnna
page_title: Use variables to configure notifications
image: /assets/img/2022/askanna-variable-notifications-vacation-version.png
image_tag: Who monitors your runs while you enjoy vacation?
excerpt: |
  Sometimes you want to (temporarily) change who should receive notifications for job activity in AskAnna. In this how
  to we show you how to do this with project variables in AskAnna.
---

Your vacation is just around the corner. Your colleague takes over the monitoring of various projects from you. In
AskAnna you can set who gets notifications. Now that you're going on vacation, you want to change this temporarily.
Also handy because when you come back from vacation, you don't have to clean up all these notification emails :)

In AskAnna you can [configure who should get email notifications](https://docs.askanna.io/job/notifications/). In the
[askanna.yml](https://docs.askanna.io/code/#askannayml) you can specify a list of email addresses. Or, you could set a variable name and use this variable to configure who should set an email.

Using the variable has the advantage that you don’t have to push a new version of the code in case you want to change who should receive a notification email.

See the `askanna.yml` below for an example. In the `notifications` section we set the variable `notification_email`. With this set in the job definition, every time you run the job, it will look up this variable and add the value to the list of email addresses that should receive a notification.

```yaml
train model:
    job:
      - pip install poetry && poetry install
      - python code/train.py
    output:  
      result: model.pkl
    schedule:  
      - “@weekly”
    notifications:  
      all:  
        email:  
          - ${notification_email}
```

With the job configured, you only have to add this variable to the project. You can use the AskAnna CLI:

```shell
# Add a new variable
askanna variable add -n notification_email

# Change an existing variable
askanna variable change
```

Or:

- open your project in the web interface,
- go to the [project section variables](https://docs.askanna.io/variable/#web-interface)
- and add a variable with the name `notification_email`.

The value for this variable can be one or multiple email addresses. If you want to add multiple email addresses, you
can comma-separate them: `info@example.com, hello@example.com, ...`

{% image 2022/askanna-variable-page.gif alt="Add and update variables in AskAnna" %}

Now you know how to use variables in AskAnna to change who should receive a notification about job activity quickly.
