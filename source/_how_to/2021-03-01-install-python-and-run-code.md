---
title: Install Python and run code - AskAnna
page_title: Install Python and run code
---

Before we can start learning "how to run jobs in AskAnna", you need to be able to run Python code on your local device.
On this page we will help you to run Python code.

First, we will talk about installing Python and Visual Studio Code. Then we help you start running a Python script, and
we end with further references that might help you out to kickstart your first Python project.

## Install software

To install Python, you can use a distribution like [Anaconda](https://anaconda.com) or install the different packages
yourself. To learn the basics of Python, it can be useful to follow the next steps so you are more aware of what is
installed on your system.

### Python

To be able to run Python code on your machine, you need to install Python. You can go to
[https://www.python.org/downloads/](https://www.python.org/downloads/). Select the installation matching your Operating
system. Probably the site already detected your Operating System, so you can simply click `Download Python x.y.z`.
After downloading, you can start the installation of Python.

### Visual Studio Code

It's possible to build your first Python project using a text editor. But tools like
[PyCharm](https://www.jetbrains.com/pycharm/) and [Visual Studio Code](https://code.visualstudio.com/) can make it a
lot easier to work on your project.

In this document we use [Visual Studio Code](https://code.visualstudio.com/). It is used a lot, and it is free. For
Python projects, [PyCharm](https://www.jetbrains.com/pycharm/) might have more options. But to be able to use them, you
should buy a license. For most use cases, you will be happy with Visual Studio Code.

Go to [Visual Studio Code](https://code.visualstudio.com/) to download the software. Next, install it.

In Visual Studio Code you can install extensions that support you on your project. For Python projects the
[Python extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python) is usefull. If you installed
Visual Studio Code, also [install the Python extension](vscode:extension/ms-python.python).

### Optional: Anaconda

It’s not necessary to install Anaconda. Some data scientists love it because if you install Anaconda, you get many data
science tools out of the box. For example, it will install popular tools like Pandas and JupyterLab.

If you want to go the easy way, you can decide to install Anaconda. Go to the
[Anaconda Individual edition](https://www.anaconda.com/products/individual#Downloads) and download the version matching
your operating system. After downloading, you can install the tool.

## Run your first Python script

You have installed Python and set up Visual Studio Code. Now it's time to create your first project. When you open
Visual Studio Code on the welcome page you can select `New file`. Let's first save the file. Choose a directory and
save the file with the extension `.py`. I used `my-first-python.py`.

Now you have a Python file, and in this file we can write our script. Copy the next line, paste it into your Python file
and save it:

```python
print('Hello AskAnna')
```

This Python function will print the text `Hello AskAnna`. If you want to run this Python code, you can click on the
<i class="fa fa-play"></i>-button in the right upper corner. This will open a terminal and run your Python code.

{% image 2021/run-python-code.png alt="Run Python code in Visual Studio Code" %}

You will see the command referring to your Python interpreter & the Python file it runs in the terminal. And it will show the output `Hello AskAnna`.

You can also run the code yourself via the terminal. In your terminal, make sure you are on the directory where you saved the Python file. Now you can run:

```bash
python my-first-python.py
```

Now you should see the same output.

This is just a starter. If you want to run more SimplePrograms, check the
[SimplePograms on Python Wiki](https://wiki.python.org/moin/SimplePrograms). Copy the examples in your Python script,
and run the code as described above. Have fun learning Python!

## Learn more

Soon we will post more "How to" content to help you out kickstart your data (science) project. Do you want to learn
how to explore your data, connect to a database and run a model? We will write about it.

If you have a specific question you want to get to know more about, let us now [ask@askanna.io](mailto:ask@askanna.io)!

And when you can't wait, maybe you can find inspiration on one of the next pages:

- [Python Wiki](https://wiki.python.org/)
- [Python Tutorial](https://pythonbasics.org/)
- [The Hitchhiker's Guide to Python](https://docs.python-guide.org/intro/learning/)
- [Real Python Tutorials](https://realpython.com/)
- [Full Stack Python](https://www.fullstackpython.com/)
