---
title: Get AskAnna up and running in 15 minutes
page_title: Up and running in 15 minutes
featured: True
image: /assets/img/15mins.png
image_header: True
image_tag: AskAnna is up and running in 15 minutes
---

The project "Hello AskAnna" is an advanced demo project. The goal is to give you a quick overview of how easy it is to
set up a project in AskAnna. In the "Hello AskAnna" project, we show how you can:

* create a project
* write a "Hello AskAnna" Python script
* define a job that can run the script
* push the code to AskAnna
* run the job in AskAnna

## 01. Create the project

We assume you have an AskAnna account and that you logged in. On the workspace overview, you can quickly create a
project via the button `Create project`. You only have to add a name, and you can create the new project.

{% image 2021/askanna-hello-create-project.png alt="Create a project in AskAnna" %}

After you created the new project, you can open it.

## 02. Hello-script

On your local device, you can create a directory for your project. In this directory you can add a simple Python
script. For example:

```python
print("Hello, AskAnna!")
```

Save this file as `hello.py`.

## 03. AskAnna configuration

In your local directory add a file `askanna.yml`. In this file we will add the push-target and configure the job to run
the Hello-script in AskAnna.

### Push target

Via [the push-target](https://docs.askanna.io/code/#askannayml), you inform to which project in AskAnna you want to
push the code. In step 01 we created the project. If you open this project, you see instructions about how to add code
to AskAnna. Here you can find the push-target. Simply click on the copy button next to it and paste this line into your
`askanna.yml` file.

{% image 2021/askanna-hello-push-target.png alt="AskAnna Push target" %}

### Job definition

In the `askanna.yml` we will also define a job `hello` that can run the hello-script. Add the next lines to your
`askanna.yml` config file:

```yaml
hello:
  job:
    - python hello.py
```

Now your `askanna.yml` file should contain the push-target and the definition of the job. We are now ready to push the
code to AskAnna.

## 04. Push code to AskAnna

In your Python environment, you should install AskAnna by running:

```shell
pip install askanna
```

Ones installed, you can log in by running:

```shell
askanna login
```

In the directory where you build the "Hello AskAnna" project, you can now run the command to push your code to AskAnna:

```shell
askanna push
```

Now the AskAnna CLI will package your code and upload this to AskAnna. After the upload is finished, you can open your
project in AskAnna. Now you should see two files. Feel free to click on a file to check the content.

{% image 2021/askanna-hello-code.png alt="AskAnna Code" %}

## 05. Run the Hello-job

If you now click on the `JOBS` section, you will see the job `hello`:

{% image 2021/askanna-hello-job.png alt="AskAnna Job" %}

Click on the job to open the job page. Here you can add a description and also find a section `Running the job`. For
now, we will run the job via the platform. You can do this by removing the example JSON data (or not because it doesn't
matter) and press `RUN THIS JOB`:

{% image 2021/askanna-hello-run-job.png alt="Run a job in AskAnna" %}

Click on `OPEN THE RUN` to open the run page. On this page you can find meta-information about the run. When the run
finished, you can open the `LOG` section and verify that AskAnna run your code:

{% image 2021/askanna-hello-run-log.png alt="Log of a run in AskAnna" %}

## Congratulations!

You now have successfully:

* created a new project in AskAnna
* configured a job
* pushed code to AskAnna
* run a job
* opened the run in the web interface
