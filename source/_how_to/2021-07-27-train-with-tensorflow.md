---
title: Train a model with TensorFlow
external: https://docs.askanna.io/examples/train-with-tensorflow/
excerpt:
  Learn how to train a neural network model to classify images of clothing. In this example, we use TensorFlow.
---
