---
title: Use locale in Python
external: /journal/2022/use-locale-in-python/
excerpt:
  Did you know you can easily switch to local formats using Python's locale? A quick intro in how you can do this.
---
