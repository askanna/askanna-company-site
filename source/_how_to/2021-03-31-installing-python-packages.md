---
title: Installing Python packages - AskAnna
page_title: Installing Python packages
excerpt: Install additional Python software via pip. In this how-to you learn not only how to install the packages, but also how to manage installed packages.
---

One of the pros of working with Python is that many people and organizations contribute to the ecosystem by making
Python software available as a package. You can see a package as a Python plugin you can optionally install. In this
how-to, we will give you a brief introduction of how you can install and manage it.

Before we start installing the first packages, a tip to activate a (new) virtual environment. In the how-to
["Using virtual environments"](../virtual-environments/) you can read how to do this. You can run the commands provided
in this how-to in your terminal.

Most packages can be installed using pip. Pip stands for `Pip Installs Packages`. There are other methods, but that's
out of scope for this kickstart how-to.

## Check if pip is installed

When you [installed Python](../install-python-and-run-code/), pip is probably installed on your system as well. If the
Python installer includes pip, it will also be available in your virtual environment. But let's check to be sure that
pip is available by running:

```bash
pip --version
```

If somehow pip is not installed, you can use the GitHub project ["get-pip.py"](https://github.com/pypa/get-pip) to
install pip. You can also [download the `get-pip.py`](https://bootstrap.pypa.io/get-pip.py) file and run the next
command from the location where you saved the downloaded file:

```bash
python get-pip.py
```

## PyPI

[The Python Package Index (PyPI)](https://pypi.org/) is a central location with packages for Python. Most of the Python
software is published on this site. You can search for projects, check the release history and other relevant
information. For example, [the AskAnna CLI & Python SDK](https://pypi.org/project/askanna/) is published here as well.

When you are working on data science projects, some of the popular packages you might want to use are:

- [Pandas](https://pypi.org/project/pandas/)
- [Scikit-learn](https://pypi.org/project/scikit-learn/)
- [TensorFlow](https://pypi.org/project/tensorflow/)

## Install packages with pip

When you install a package with pip, it will check [PyPI](https://pypi.org/) if the package is listed and install the
software. If you want to install the latest version of [Pandas](https://pandas.pydata.org/), you can run:

```bash
pip install pandas
```

Sometimes you need to install a specific release. To do this, you should add the version number of the release you want
to install:

```bash
pip install pandas==1.2.3
```

If you want to see the packages installed in the current environment, you can list them by running:

```bash
pip freeze
```

## Using a requirements.txt file

It's a good habit to use a virtual environment for your Python project. Another good habit is that you install packages
using a `requirements.txt` file. Using the `requirements.txt` file, you can share with the team which packages are used
for the project. You can version control this file and also list the version of the packages used in the file. By
doing this, you make sure that if others run the project it works as intended.

A `requirements.txt` file contains a list of packages. One per line. It can look like this:

```text
pandas
scikit-learn
askanna
```

If you want to specify the package version, you can do this by adding the version number:

```text
pandas==1.2.3
scikit-learn==0.24.1
askanna
```

Note that for the AskAnna package we did not add the version number. If we list it like this, we will always install
the latest version for AskAnna in case AskAnna is not yet installed. Other notations to specify which version of a
package should be installed are:

- `>=`: the minimum version that should be installed
- `~=`: if a new patch is available, install the latest patched version

If you want to install the packages in the file `requirements.txt`, you can run the next pip command:

```bash
pip install -r requirements.txt
```

Every time you run this command, pip will check if it is necessary to install a new package or upgrade an existing one.

If you installed packages directly with pip, and later realize you want to create a `requirements.txt` file you can run:

```bash
pip freeze > requirements.txt
```

With `pip freeze` you will create a requirements.txt file or overwrite the existing file. This file lists all the
packages currently installed in the active environment. Also installed dependencies will be listed. Impact is that
although you will get a list of all packages installed, you don't know which packages you installed and which packages
where dependencies of the packages you installed. So if you ask me, it's better not to use `pip freeze` and install the
packages always using a `requirements.txt` file.

## Need more

This was a quick introduction to installing Python packages. If you have a question, you can always send an email to
[ask@askanna.io](mailto:ask@askanna.io). You can read more about pip and Python packages on the
[official pip project](https://pypi.org/project/pip/) and the
[Python Packaging User Guide](https://packaging.python.org/tutorials/installing-packages/).
