---
title: Train, select and serve a model
external: https://docs.askanna.io/examples/train-select-serve-model/
excerpt:
  An example that uses wine to tell you all about training and serving data science. It shows how you can train a
  model, select the best, and use that model to serve a prediction.
---
