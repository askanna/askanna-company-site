---
title: Read from and write to PostgreSQL with Python Pandas
external: https://docs.askanna.io/integrations/data-sources/postgresql/
excerpt:
  In this example we show how to set up a connection to a PostgreSQL data source using Python.
---
