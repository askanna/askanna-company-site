---
title: How to add & version control project code with AskAnna
page_title: Add & version control project code
excerpt: Add and version control code to your project in AskAnna by uploading it via the webpage, push it using the terminal or automatically after a commit with GitLab or GitHub.
image: /assets/img/2021/add-your-code.png
image_header: False
---

With AskAnna, it’s possible to version control your project code. This is part of the solution where AskAnna makes it
possible to track everything you do, so you can reproduce what you have done.

Our goal with tracking code is that when you run a job, it is possible to trace back the code used to run that job. We
are not a replacement for code management platforms like [GitLab](https://gitlab.com/) or
[GitHub](https://github.com/). But it's possible to integrate with these platforms. This way you can use GitLab/GitHub
for code approvals and AskAnna to manage your runs. We will end this how to by showing
[how you can set up this integration](#deploy-code-via-gitlab-or-github).

Because not everyone is using git, we will first start by adding code to your AskAnna project without git. We will show
how you can do this by uploading via the AskAnna web interface and push code via your terminal. Before we continue,
make sure you [created a project](https://docs.askanna.io/project/#create-a-project) in AskAnna to try it out
directly. If you are looking for a demo project, you can download
[this zip file](/assets/demo/demo-multiple-models.zip). It contains a demo project but without the
[push-target](https://docs.askanna.io/code/#push-target) set. In this how to, you will also read when you "need" to
[set the push-target](#push-it).

## Upload a Zip file

If you want to use the web interface to upload your code, you can do this by making a zip archive of your local project
directory. In case you already have defined jobs in the `askanna.yml` file, make sure that the `askanna.yml` is in the
root directory of this zip archive. To try it out, you can also use
[this zip archive containing a demo project](/assets/demo/demo-multiple-models.zip).

If you create a new project in AskAnna, you should see a screen like below. Because the new project does not contain
code yet, AskAnna shows you instructions of how you can add code. With the web interface you have the next two options.

**Click on browse** and select the zip archive you want to upload. After selecting the zip archive, the browse button
will become an upload button. Click on the upload button and confirm you indeed want to upload the code. After
confirming, AskAnna will upload your code. After uploading, you can check the uploaded code.

{% image 2021/add-code-upload-zip.png alt="Upload a zip to AskAnna" %}

**Drag & drop** the zip archive to the dotted area. Drop it, and you will see the file appear on the page. Also the
browse button is now an upload button. From here it's the same as written above in *click on browse*.

Now you have successfully uploaded your code. For the demo project you will see:

{% image 2021/add-code-view.png alt="View uploaded/pushed code in AskAnna" %}

Besides the code you just uploaded, you see three buttons. You can `download` this version of the code as a zip
archive. Also you can `replace` the code with a new version. Click on this button, and you will find similar
instructions as you find above.

You can also check previous added code if you click on the `history` button. On the history page you can find which 
code versions you had. Every job you run in AskAnna will be linked to the version of the code used. On the history page
of the code you can also find previous versions of the code. This is an easy way to **version control** your project
code.

## Push it

Although uploading a zip archive is a way to go, I think that using the command line to push code to AskAnna is the
fastest way. To push your code to AskAnna, you need to add a file `askanna.yml` to the root folder of your project. Add
the next line in this file: `push-target: ${ PROJECT URL }`. If you are on the project page, you can also copy the
project URL from the code page. See the next image:

{% image 2021/add-code-push-target.png alt="Push target for an AskAnna project" %}

Besides adding the `askanna.yml` file, you also need to install AskAnna. If you did not install AskAnna yet, make sure
you have [installed Python](/how-to/2021/install-python-and-run-code/). Next, install AskAnna by running
`pip install askanna` in your terminal.

Now you are ready to push code to the project in AskAnna. In the terminal run:

```bash
askanna push
```

If you now reload the project page on AskAnna, you will see the code you just pushed. When you want to replace the
current code, run the same command and you will upload the latest code version. Optionally you can also add a
description to the new version:

```bash
askanna push -m "a description"
```

**Tip:** if you use Visual Studio Code, you can use the "internal" terminal. In the menu bar, select the option
`Terminal`. Next, choose the option to open a new terminal. The terminal will open at the project root directory. Now
you can run `askanna push`, while continue working on your project in Visual Studio Code.

## Deploy code via GitLab or GitHub

In case your team already uses GitLab or GitHub for managing code, you might be interested in setting up a deploy job
that will push the code automatically. The example code below is with the idea that when you commit to the main branch,
a GitLab job or GitHub action will push that version of the code to AskAnna.

The project on GitLab/GitHub should contain an `askanna.yml` file with the push-target set. This is similar to the
configuration you need to push the code from your local machine. On [Push it](#push-it) you can read how to do this.

To authenticate, you use the token of a workspace member. This can be your own token, but you can also add a new
workspace member that you can use for this kind of authentication. At AskAnna we use for example a member Mr. Robot.

If you installed AskAnna via pip and logged in, you can find the token in the local AskAnna configuration file. On
Mac/Linux you can view the file in a terminal via:

```bash
cat ~/.askanna.yml
```

On Windows you can find the `.askanna.yml` configuration file in your user home directory.

You need the token in the project configuration of GitLab/GitHub. Next you will read for each platform how to configure the
autopush to AskAnna.

### GitLab CI

Let's first add the token as a variable to the project. You can find the variables by going to:

- Open the GitLab project
- Hoover on `Settings` (left side menu bar)
- Select `CI/CD`
- Expand `Variables`
- Click `Add Variable` and add a variable use the next information:
  - Key: `AA_TOKEN`
  - Value: the copied token
  - Type: Variable
  - Environment scope: All (default)
  - Flag `Mask variable`

In the project code, you can add a file `.gitlab-ci.yml` or edit an existing version of the file. Add the next GitLab
CI job to the file. This job installs AskAnna and pushes the latest version of the code when there is a new commit in
the main branch.

```yaml
# This is a GitLab job to push code to AskAnna after a new commit to main branch

Update AskAnna project:
  stage: deploy
  image: python:3-alpine
  script:
    - apk add git
    - python -m pip install --upgrade pip
    - pip install askanna
    - askanna push --force
  only:
    - main
```

That's it for GitLab. If you set up the above configuration, every time there is a commit to the main branch it will
trigger a CI/CD job to push the code to the AskAnna project.

### GitHub Action

Similar to GitLab, let's first add the token as a secret to the project. To add the token in GitHub, we need to set a
secret for the project via:

- Open the GitHub project
- Click on `Settings`
- On the left side menu select `Secrets`
- Click on `New repository secret`
- Add a new secret with:
  - Name: `AA_TOKEN`
  - Value: the copied token

In the project code, you can add a GitHub workflow file: `.github/workflows/update-askanna.yml`

In this file you can configure the job to push code to AskAnna. Copy past the next lines to the workflow file:

```yaml
# This is a GitHub workflow to push code to AskAnna after a new commit to main branch

name: Update AskAnna project

on:
  push:
    branches: [ main ]

jobs:
  deploy:
    runs-on: ubuntu-latest

    steps:
      - uses: actions/checkout@v2
      - name: Set up Python
        uses: actions/setup-python@v2
        with:
          python-version: '3.9'
      - name: Install dependencies
        run: |
          python -m pip install --upgrade pip
          pip install askanna
      - name: Push code to the AskAnna project
        env:
          AA_TOKEN: ${{ "{{ secrets.AA_TOKEN " }}}}
        run: askanna push --force
```

That's it for GitHub. If you set up the above configuration, every time there is a commit to the main branch it will
trigger a GitHub action to push the code to the AskAnna project.

## Next: create and run a job

AskAnna is more fun when you use it to run your jobs. In the documentation you can read more about how to [create jobs](https://docs.askanna.io/job/create-job/), and [run them](https://docs.askanna.io/job/run-job/) in AskAnna.
