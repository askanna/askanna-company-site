---
title: Run and compare multiple models
external: https://docs.askanna.io/examples/multiple-models/
excerpt:
  This how to is all about running code in AskAnna. It shows how you can train and evaluate multiple models using AskAnna.
---
