---
title: The AskAnna Promise
description: AskAnna makes complex matters simple and intuitive to use so you can be in control of your data science projects.
show_followus: True
---

## We make complex matters simple and intuitive to use so you can be in control of your data science projects

The world of data science, machine learning, artificial intelligence and data engineering is changing rapidly. It can be a challenge to find the optimal model fit. And to run your code in the cloud, you need to set up a (complex) infrastructure. To exploit the maximum potential, we must make this more intuitive to use. Luckily, at AskAnna we love to keep things simple. We love to use the power of IT to make this part of the job fast and straightforward for you.

We think that together we can even make more things in the field of data science simple. Data science is, in the end, a matter of setting up a connection to your data sources, running a set of scripts, storing the results and sharing it with your team or organization. Also when you run your data science in the cloud, it should be as simple as this.

So ask yourself some questions. Why do I build all these complex pipelines to run my model? Is this really necessary for my project? We are convinced that this is not the case. And that’s why we built AskAnna. Instead of managing a complex software architecture, let’s go back to a straightforward solution and still do what you need.

We help you by giving you back your time, so you have more time to play with your data & models!

&nbsp;

## Track what you do

To be in control, you need to be able to track what you do. For every run, you want to know the input & output, which code was used, the parameter settings and other relevant metadata like metrics & issues. This is so important that with AskAnna you can do this out of the box. You don’t have to set up a shared filesystem or new database environment, but you can use an AskAnna project to track the work done. We made it so easy that you can start tracking your work when you start your first experiment.

## Trace back what you did

With AskAnna, you can quickly name and label your runs, so you have a logical way to collect your successes...or learnings. Also, we built an intuitive interface where you can easily trace the work done. Or you can use the AskAnna SDK so you can work from tools like Jupyter Notebooks. We make it easy for you to trace back what you did, that you can use the tools you love and that you can retrieve all tracked information from your runs.

***Note:** Labels are not available yet in the Beta version of AskAnna. Coming soon!*

## Compare your runs

When you work on an algorithm and try to figure out the best model fit, you probably also want to compare your runs. Only, it’s annoying and time-consuming to set up the infrastructure needed for every project. That’s why we built an interface that empowers you to compare runs quickly and spot the differences between them. The only thing you need to do is track your runs in AskAnna. Compare your runs are directly available. It’s so simple that you will love to use it.

## Share it with others

The most common way of sharing something online is by simply sharing a link. So that’s what we did at AskAnna. You can share every run with a unique link, so your teammate can directly open the result you are referring to. And because you can invite unlimited people, you can share it with everyone you want.

## You don’t have to be a cloud expert

Sometimes running a script can take a lot of time. Although you know running it on your laptop is not the best solution, you also don’t want to lose time setting up a cloud server, build a Docker image and all the debugging that comes with it. We think running your script in the cloud should be as simple as pushing your code to AskAnna and telling the platform which script it should run. AskAnna will take care of setting up the infrastructure needed to run your script. Using AskAnna will save you a lot of time and frustration. And imagine how many experiments you can run simultaneously. We will help you to work more efficiently.

## Be connected

Building a data science model is, in most cases, part of a bigger solution where different tools need to work together. At AskAnna we embrace REST API design. By doing so, we make it possible for you to easily integrate AskAnna into existing pipelines while your data science team keeps working from the AskAnna platform. You can quickly monitor results yourself when the project moves from proof of concept to production. Or you can quickly deploy an improved version of your model. You can keep control of your project.

## Don't pay too much

Running services in the cloud can become pricey. Maybe you experienced forgetting to turn off a high-performance server after training your model. What did you think of the bill? You only want to run a job and stop paying for the calculation power when the job is done. It should be as simple as this, so that’s how we do it in AskAnna. You only pay for the runtime you use, next to a fixed monthly price for unlimited projects and users. We don’t want you to pay too much for the service you need to do your work.

## Keep your freedom

Nowadays, many tools, packages, software languages and plugins are available to run your data science projects. We built AskAnna to serve your data science projects. With AskAnna, you can run other tools as well. It's like Lego. You are flexible to use the blocks you need or want to try out. You can combine these Lego blocks and use AskAnna to manage your results.

## You own your data

Of course, we will never use your data. But with "you own your data", we mean more. We never want to force you to keep using AskAnna. We mentioned Freedom. This includes you will have the freedom to host your data science solution somewhere else. You can always export all your information from AskAnna and still use it. Of course, we do everything to give you the best experience, so you never want to say goodbye.

## Keep it simple, stupid

We want to make running a data science project easy, fast and intuitive. It should be fun and not an overly complex architecture where you need an MLOps team to support you. For a data science project, you want to connect to data sources, prep the data, train models, serve results and share the work. This is an easy flow. With AskAnna we empower you to work as simple as this. Keep it simple, stupid!

## Save time

We want you to go fast. Don't spend days setting up an infrastructure, but start right now. Don't spend time on meetings with your IT architects, but start running your jobs in AskAnna now. And by using the track, trace and compare setup of AskAnna, your work is structured. Also, when you are ready to integrate your solution, it will be fast and easy. We will save you a lot of time, so you have more time to do what you love to do: building fantastic models and getting the maximum out of your data.

## Long haul

We think carefully about what we do and how we want to do it. This means that if we introduce a tool or solution, we always will support it for the long term. We probably all have experience with providers who suddenly stop with an application. Providers like Google and Microsoft kill products all the time. Not at AskAnna. We commit ourselves to support and improve our products as long as you use them. Nice saying, but we can also prove it with our track record of previous products we still maintain. We are proud about doing the right thing for you!

## Ask Anna Anything

You can ask and discuss anything with us. Simply send us an email: [hello@askanna.io](mailto:hello@askanna.io). We can't promise that we can help you with everything ;) What we promise is that you always will get an honest answer from us. And we want to build AskAnna together with you. Share your feedback and ideas, but also use the power of our community. We have seen a lot, so we might be able to help you. We want you to succeed in your data science projects and to keep things simple. Feel free to ask us anything...especially when it's about data science!
