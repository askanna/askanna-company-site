---
title: March 2021
---

## Track Metrics

When you run a job, you can now track metrics in AskAnna. It's as simple as adding two lines to your Python file:

```python
from askanna import track_metric

track_metric(name="accuracy", value=accuracy)
```

A variety of data types are supported, and you can add labels. Labels can be useful for situations where you run
different scenarios, want to compare different models, et cetera.

Want to analyze your run, or make a report in your Notebook? Via the Python SDK you can get the run metrics.

```python
import askanna

run = askanna.run.get(run="4Xo4-n1Wz-ky73-Nhe7", include_metrics=True)
accuracy = run.metrics.get("accuracy")
```

And you can view the metrics in the web interface as well:

{% image 2021/metrics_run-page-metrics.png alt='Track Metrics in AskAnna' %}

<center>
    <a href="https://docs.askanna.io/metrics/" class="promise--cta pill">
    Read more about Track Metrics
    </a>
</center>
