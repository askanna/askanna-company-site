---
title: April 2022
---

## Viewing CSV files

If you have CSV files in your project, you probably know that opening CSV files is not always handy. Most of the time
you get the “raw” content which is not easy to read.

With this update of AskAnna, we help you. When you open a CSV file, you get a pretty view. We present the file content
in a table.

And you can search through the records. Or, you can sort and group by on one of the columns.

{% image 2022/askanna-csv-files.gif alt='Viewing CSV files' %}
