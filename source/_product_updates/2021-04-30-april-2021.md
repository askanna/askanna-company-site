---
title: April 2021
---

## Schedule your Job

Sometimes you have a job that you want to run frequently. Now you can add a schedule to your `askanna.yml`
configuration:

```yaml
name of the job:
  job:
    - ...
  schedule:
    - "@weekly"
```

We support multiple schedules, and you can set the timezone:

```yaml
name of the job:
  job:
    - ...
  schedule:
    - "@weekly"
    - day: 1
    - "0 8 * * *"
```

When you add a schedule to your job, you can check the schedules on the job page. Also you can see when the next run is 
planned:

{% image 2021/schedule.png alt='Schedule your Job in AskAnna' %}

<center>
    <a href="https://docs.askanna.io/job/schedules/" class="promise--cta pill">
    Read more about Schedules
    </a>
</center>

## Track Variables

When you run a job, you can now not only track metrics but also track variables. It's as simple as adding two lines to your Python file:

```python
from askanna import track_variable

track_variable(name="model name", value="XGBoost")
```

Similar to track metrics, you can also get the tracked variables using the Python SDK or view them in the web interface:

{% image 2021/variables_run-page-variables.png alt='Track Variables in AskAnna' %}

<center>
    <a href="https://docs.askanna.io/variable/tracking/" class="promise--cta pill">
    Read more about Track Variables
    </a>
</center>
