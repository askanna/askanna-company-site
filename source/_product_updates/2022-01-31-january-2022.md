---
title: January 2022
---

## Editor update: add links & full screen mode

You can now use links in your run, jobs and project descriptions.

And because your descriptions can be long, we have also added a full screen option. Use all the space to focus on what
you want to share.

{% image 2022/askanna-demo-update-editor.gif alt='Do more with the editor in AskAnna' %}

## Copy images

You can now copy images in AskAnna.

{% image 2022/askanna-copy-image.png alt='Copy images in AskAnna' %}
